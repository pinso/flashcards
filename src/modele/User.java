package modele;
/**
 * 
 * Une classe qui cree un user.
 * @deprecated cette classe n'a pas eu d'utilite dans le projet.
 *
 */
public class User {

	private String pseudo;
	private Boolean estRoot;
	private final String mdpRoot="root";

	public User(String p) {
		pseudo=p;
		if (p.equals("root")){
			this.estRoot=true;
		}
		else {
			this.estRoot=false;
		}
	}

	public String getPseudo() {
		return this.pseudo;
	}

	public void setPseudo(String p) {
		this.pseudo=p;
	}

	public Boolean getEstRoot() {
		return this.estRoot;
	}

	public void setEstRoot(Boolean er) {
		this.estRoot=er;
	}
}
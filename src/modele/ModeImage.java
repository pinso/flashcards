package modele;

import java.awt.BorderLayout;
import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import controler.ControlerSouris;
import main.Start;
import vue.VueImage;
/**
 * 
 * Interface du mode de jeu image.
 *
 */
public class ModeImage extends ModelJeu {
	/**
	 * Attribut jeu qui represente le main dans lequel la classe est utilisee.
	 */
	private static Start app;
	/**
	 * Le deck de cartes que l'on utilisera pour le jeu.
	 */
	private DeckCarte dc;
	/**
	 * Conteneur qui contiennent les cartes.
	 */
	private JPanel carte, plateau, panelres;
	/**
	 * La ligne d'une carte ou sera stockee la carte a deviner.
	 */
	private static GridLayout grille = new GridLayout(1,1);
	/**
	 * L'id de la carte a deviner.
	 */
	private int cadeviner;
	/**
	 * Nombre de carte dans la partie parmis choix reponses.
	 */
	private int nbcarte;
	/**
	 * Nom du fichier image de la carte.
	 */
	private String chemin;
	/**
	 * Chemin absolu ou se trouvent les cartes.
	 */
	private String repertoire = "src/img";
	/**
	 * Conteneur qui contient l'image de la carte a afficher.
	 */
	private JLabel lab;
	/**
	 * Carte independante qui nous permet de recuperer le compteur.
	 */
	private CarteImage cardcount;
	/**
	 * Permet l'affichage du nombre d'erreur et de cartes trouvees.
	 */
	private JLabel erreur, trouvee;
	/**
	 * Bouton pour changer retourner a l'accueil
	 */
	private JButton accueil;
	/**
	 * Constructeur de la classe ModeImage
	 * @param i, le nombre de cartes avec lesquels on souhaite jouer.
	 * @param cc, la carte qui nous servira a recuperer le compteur.
	 * @param css, le controleur qui nous permet de recuperer les actions de la souris.
	 */
	public ModeImage(int i, CarteImage cc, ControlerSouris css) {
		
		this.cardcount = cc;
		cardcount.reinit();
		
		this.nbcarte = i;
		/*************************************
		 * Creation des composants Model et controleurs.
		 *************************************/
		dc = new DeckCarte();
		cs = css;
		cs.setDeck(dc);
		dc.setModeActif(this);
		
		/*************************************
		 * Le JPanel plateau au nord dans lequel les cartes sont affichees.
		 *************************************/
		plateau = new JPanel();
		carte = new JPanel();
		panelres = new JPanel();
		plateau.setLayout(grille);
		grille.setHgap(10);
		grille.setVgap(10);
		
		
		erreur = new JLabel();
		trouvee = new JLabel();
		
		accueil = new JButton("Accueil");
		
		accueil.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				removeContent();
				app = new Start();
				
				app.getContentPane();
			}
		});
		
		this.changerParam(dc.getNbErreur(), dc.getNbCartesTrouvees());
		
		/*************************************
		 * G�n�ration du nombre de cartes demand�es en param�tre
		 *************************************/
		dc.chargerImage(i);
		/*************************************
		 * Creation des JLabel associes aux cartes + affichage dans le JPanel .
		 ************************************/
		this.cadeviner = (int)(Math.random()*this.nbcarte);
		dc.setDeviner(dc.getCarte(this.cadeviner).getIdCarte());
		lab = new JLabel();
		String chemin2 = repertoire + "/fond" + dc.getCarte(this.cadeviner).getIdCarte()+".png";
		try {
			lab.setIcon(new ImageIcon(ImageIO.read(new File(chemin2))));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		carte.add(lab);		
		
		
		
		for(int j = 0; j<this.nbcarte; j++) {
			if(dc.getCarte(j).estVisible()) {
				dc.getCarte(j).setVisible(true);			
			}
			chemin = repertoire + "/im" +dc.getCarte(j).getIdCarte()+".png";
			VueImage vc = new VueImage(dc.getCarte(j), chemin);
			vc.addMouseListener(cs);
			dc.addObserver(vc);
			try {
				vc.setIcon(new ImageIcon(ImageIO.read(new File(chemin))));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			plateau.add(vc);
		}
		
		/*************************************
		 * Construction de l'IG dans une JFrame.
		 *************************************/
		panelres.setLayout(new GridLayout(1,5));
		panelres.add(erreur);
		panelres.add(trouvee);
		panelres.add(new JLabel());
		panelres.add(new JLabel());
		panelres.add(accueil);
		
		this.setLayout(new BorderLayout());
		this.add(panelres, BorderLayout.NORTH);
		this.add(carte, BorderLayout.CENTER);
		this.add(plateau, BorderLayout.SOUTH);
		
	}
	
	/**
	 * Methode pour effacer le contenu du JPanel
	 */
	public void removeContent() {
		Component comp = SwingUtilities.getRoot(this);
		((Window) comp).dispose();
	}
	
	/**
	 * Methode qui permet de changer l'affichage du nombre d'echec et de cartes trouvees.
	 * @param er, entier representant le nombre d'erreur.
	 * @param tr, entier representant le nombre de cartes trouvees.
	 */
	public void changerParam(int er, int tr) {
		erreur.setText("Nombre d'erreur : "+er+"/3");
		trouvee.setText("Cartes trouvees : "+tr);
		this.revalidate();
		this.repaint();
	}
	/**
	 * La methode qui permet de recharger d'autre carte pour faire une nouvelle partie.
	 */
	public void nouvellePartie() {
		dc.chargerImage(this.nbcarte);
		
		/*************************************
		 * Le JPanel plateau au nord dans lequel les cartes sont affichees.
		 *************************************/
		plateau.removeAll();
		carte.removeAll();
		plateau.setLayout(grille);
		grille.setHgap(10);
		grille.setVgap(10);
		/*************************************
		 * Creation des JLabel associes aux cartes + affichage dans le JPanel .
		 ************************************/
		this.cadeviner = (int)(Math.random()*this.nbcarte);
		dc.setDeviner(dc.getCarte(this.cadeviner).getIdCarte());
		lab = new JLabel();
		String chemin2 = repertoire + "/fond" + dc.getCarte(this.cadeviner).getIdCarte()+".png";
		try {
			lab.setIcon(new ImageIcon(ImageIO.read(new File(chemin2))));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		carte.add(lab);		
		
		
		
		for(int j = 0; j<this.nbcarte; j++) {
			dc.getCarte(j).setVisible(true);		
			chemin = repertoire + "/im" +dc.getCarte(j).getIdCarte()+".png";
			VueImage vc = new VueImage(dc.getCarte(j), chemin);
			vc.addMouseListener(cs);
			dc.addObserver(vc);
			try {
				vc.setIcon(new ImageIcon(ImageIO.read(new File(chemin))));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			plateau.add(vc);
		}
		
		/*************************************
		 * Construction de l'IG dans une JFrame.
		 *************************************/
		
		this.revalidate();
		this.repaint();
		
	}
	
}


package modele;
/**
 * 
 * Interface Deck qui a pour objectif de regrouper toutes les objets de classe Carte.
 *
 */
public interface Deck {
	/**
	 * Methode qui permet de melanger la liste de cartes.
	 */
	public void melanger();
	/**
	 * Methode getter qui permet de retourner le nombre de coup joue.
	 * @return un entier qui represente le nombre de coups joue par l'utilisateur
	 */
	public int getNbCoupJoues();
	/**
	 * Methode getter qui permet de retourner le nombre de carte trouvee par l'utilisateur
	 * @return un entier qui represente le nombre de carte trouvee par l'utilisateur
	 */
	public int getNbCartesTrouvees();
	
}



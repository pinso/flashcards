package modele;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controler.ControlerSouris;
import vue.VueImage;
import vue.VueSon;
/**
 * 
 * Interface du mode de jeu son.
 *
 */
public class ModeSon extends ModelJeu {
	/**
	 * Le deck de cartes que l'on utilisera pour le jeu.
	 */
	private DeckCarte dc;
	/**
	 * Conteneur qui contiennent les cartes.
	 */
	private JPanel carte, plateau, panelres;
	/**
	 * La ligne d'une carte ou sera stockee la carte a deviner.
	 */
	private static GridLayout grille = new GridLayout(1,1);
	/**
	 * L'id de la carte a deviner.
	 */
	private int cadeviner;
	/**
	 * Nombre de carte dans la partie parmis choix reponses.
	 */
	private int nbcarte;
	/**
	 * Carte independante qui nous permet de recuperer le compteur.
	 */
	private CarteImage cardcount;
	/**
	 * Carte independante pour recuperer les chemins absolus des fichiers son.
	 */
	private CarteSon soundcard = new CarteSon();
	/**
	 * Permet l'affichage du nombre d'erreur et de cartes trouvees.
	 */
	private JLabel erreur, trouvee;
	/**
	 * Constructeur de la classe ModeSon
	 * @param i, le nombre de cartes avec lesquels on souhaite jouer.
	 * @param cc, la carte qui nous servira a recuperer le compteur.
	 * @param css, le controleur qui nous permet de recuperer les actions de la souris.
	 */
	public ModeSon(int i, CarteImage cc, ControlerSouris css) {
		this.cardcount = cc;
		cardcount.reinit();
		this.nbcarte = i;
		
		/*************************************
		 * Creation des composants Model et controleurs.
		 *************************************/
		dc = new DeckCarte();
		cs = css;
		cs.setDeck(dc);
		dc.setModeActif(this);
		
		/*************************************
		 * Le JPanel plateau au nord dans lequel les cartes sont affichees.
		 *************************************/
		plateau = new JPanel();
		carte = new JPanel();
		panelres = new JPanel();
		plateau.setLayout(grille);
		grille.setHgap(10);
		grille.setVgap(10);
		
		erreur = new JLabel();
		trouvee = new JLabel();
		
		this.changerParam(dc.getNbErreur(), dc.getNbCartesTrouvees());
		
		/*************************************
		 * Génération du nombre de cartes demandées en paramètre
		 *************************************/
		this.cadeviner = dc.chargerSon(i);
		dc.setDeviner(this.cadeviner);
		this.soundcard.setIdCarte(this.cadeviner);
		
		/*************************************
		 * Creation des JLabel associes aux cartes + affichage dans le JPanel .
		 ************************************/
		this.reponseImage();		
		this.sonDeviner();
		
		/*************************************
		 * Construction de l'IG dans une JFrame.
		 *************************************/
			
		panelres.add(erreur);
		panelres.add(trouvee);
		
		panelres.setLayout(new GridLayout(1,5));
		panelres.add(erreur);
		panelres.add(trouvee);
		panelres.add(new JLabel());
		panelres.add(new JLabel());
		panelres.add(new JLabel());
		
		
		this.setLayout(new BorderLayout());
		this.add(panelres, BorderLayout.NORTH);
		this.add(carte, BorderLayout.CENTER);
		this.add(plateau, BorderLayout.SOUTH);
		this.setVisible(true);
	}
	/**
	 * Methode qui permet de changer l'affichage du nombre d'echec et de cartes trouvees.
	 * @param er, entier representant le nombre d'erreur.
	 * @param tr, entier representant le nombre de cartes trouvees.
	 */
	public void changerParam(int er, int tr) {
		erreur.setText("Nombre d'erreur : "+er+"/3");
		trouvee.setText("Cartes trouvees : "+tr);
		this.revalidate();
		this.repaint();
	}
	/**
	 * Affiche la carte a deviner.
	 */
	public void sonDeviner() {
		String chemin = "src/img/son.png";
		VueSon vc = new VueSon(this.soundcard, chemin, this.cadeviner);
		vc.addMouseListener(cs);
		dc.addObserver(vc);
		vc.setIcon(new ImageIcon(chemin));
		carte.add(vc);	
	}
	/**
	 * Affiche les cartes reponses.
	 */
	public void reponseImage() {
		String chemin;
		String repertoire = "src/img";
		for(int i = 0; i<this.nbcarte; i++) {
			if(dc.getCarte(i).estVisible()) {
				dc.getCarte(i).setVisible(true);			
			}
			chemin = repertoire + "/im" +dc.getCarte(i).getIdCarte()+".png";
			VueImage vc = new VueImage(dc.getCarte(i), chemin);
			vc.addMouseListener(cs);
			dc.addObserver(vc);
			vc.setIcon(new ImageIcon(chemin));
			plateau.add(vc);
		}
	}
	/**
	 * La methode qui permet de recharger d'autre carte pour faire une nouvelle partie.
	 */
	public void nouvellePartie() {
		this.cadeviner = dc.chargerSon(this.nbcarte);
		this.soundcard.setIdCarte(this.cadeviner);
		/*************************************
		 * Le JPanel plateau au nord dans lequel les cartes sont affichees.
		 *************************************/
		plateau.removeAll();
		carte.removeAll();
		plateau.setLayout(grille);
		grille.setHgap(10);
		grille.setVgap(10);
		
		String chemin = "src/img/son.png";
		this.dc.setDeviner(this.cadeviner);
		VueSon vs = new VueSon(this.soundcard, chemin, this.cadeviner);
		vs.addMouseListener(cs);
		dc.addObserver(vs);
		vs.setIcon(new ImageIcon(chemin));
		carte.add(vs);	
		
		String chemin2;
		String repertoire = "src/img";
		for(int i = 0; i<this.nbcarte; i++) {
			if(dc.getCarte(i).estVisible()) {
				dc.getCarte(i).setVisible(true);			
			}
			chemin2 = repertoire + "/im" +dc.getCarte(i).getIdCarte()+".png";
			VueImage vc = new VueImage(dc.getCarte(i), chemin2);
			vc.addMouseListener(cs);
			dc.addObserver(vc);
			vc.setIcon(new ImageIcon(chemin2));
			plateau.add(vc);
		}
		
		this.revalidate();
		this.repaint();
		
	}
}


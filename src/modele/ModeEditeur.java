package modele;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import controler.ControlerDossier;
import modele.Carte;
import main.Start;

/**
 * 
 * Interface qui affiche le mode Editeur
 *
 */
public class ModeEditeur extends ModelJeu {
	/**
	 * Conteneur qui contiennent le Jtextfield et les JButton.
	 */
	private JPanel pan, pan1;
	/**
	 * Attribut jeu qui represente le main dans lequel la classe est utilisee.
	 */
	private static Start app;
	/**
	 * Des labels pour mettre du textes.
	 */
	private JLabel carte;
	/**
	 * Zone d'ecriture pour le nom de la carte
	 */
	private static JTextField nomcarte;
	/**
	 * Bouton pour changer retourner a l'accueil et acceder au fichier
	 */
	private JButton file, accueil;
	/**
	 * Controleur du modeediteur pour ajouter des cartes.
	 */
	private ControlerDossier cdd;
	/**
	 * Carte independante pour recuperer un compteur d'id.
	 */
	private CarteImage cardcount;
	/**
	 * Attribut necessaire a la lecture du fichier xml du jeu.
	 */
	private Element racine, el, id, nom,  urlImage, urlSon;
	
	/**
	 * Constructeur de la classe ModeEditeur
	 * @param cc, la carte qui nous servira a recuperer le compteur.
	 * @param cd, le controleur qui nous permet de recuperer les actions de la souris.
	 */
	public ModeEditeur(CarteImage cc, ControlerDossier cd) {
		
		this.cardcount = cc;
		cardcount.reinit();
		
		new DeckCarte();
		cdd = cd;
		cdd.setModelJeu(this);
	 
	    //Instanciation d'un objet JPanel
	    pan = new JPanel();   
	    pan1 = new JPanel();
	    
	    carte = new JLabel("Entre le nom de ta carte");
	    nomcarte = new JTextField(20);
	    nomcarte.addKeyListener(new KeyAdapter() {
	    	public void keyTyped(KeyEvent e) {
	    		if (nomcarte.getText().length() >= 7) {
	    			e.consume();
	    			JOptionPane.showMessageDialog(pan, "7 caracteres max");
	    		}
	    	}
		});
	    
	    file = new JButton(new ImageIcon("src/img/ajout.png"));
		file.setPreferredSize(new Dimension(50,50));
		
		accueil = new JButton("Accueil");
		
		file.addMouseListener(cd);
		
		pan.add(carte);
		pan.add(nomcarte);
		pan.add(file);
		pan1.add(accueil);
		pan.add(pan1);
		
	    this.add(pan);
	    this.add(pan1, BorderLayout.CENTER);
		
	    accueil.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {				
				removeContent();
				app = new Start();
				
				app.getContentPane();
			}
		});
	}
	
	/**
	 * Methode pour recuperer le texte du JTextfield
	 * @return un string correspond a la chaine de charactere 
	 */
	public static String getNom() {
		return nomcarte.getText();
	}
	
	/**
	 * Methode pour effacer le contenu du JPanel
	 */
	public void removeContent() {
		Component comp = SwingUtilities.getRoot(this);
		((Window) comp).dispose();
	}
	
	/**
	 * Methode qui recuperer le dernier id du deck et incremente de 1 automatiquement
	 * @return un entier correspondant au nouveau dernier id
	 */
	public int idNewCarte() {
		return Carte.genID.getAndIncrement();
	}
	
	/**
	 * Methode pour ecrire dans le fichier xml les nouvelles cartes qui sont generee
	 * @param compteur, correspondant au nouveau dernier id
	 */
	public void ajouterElement(int compteur) {
		SAXBuilder sxd = new SAXBuilder();
		
		Document doc = null;
		try {
			doc = sxd.build(new File("bdd.xml"));
		} catch (JDOMException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		racine = doc.getRootElement();
		
		el = new Element("carte");
		racine.addContent(el);
		List carte = racine.getChildren("carte");
		Iterator i = carte.iterator();
		
		id = new Element("id");
		nom = new Element("nom");
		urlImage = new Element("urlImage");
		urlSon = new Element("urlSon");
		
		id.setText(String.valueOf(compteur));
		nom.setText(this.nomcarte.getText());
		urlImage.setText("src/img/ima/"+this.nomcarte.getText()+".png");
		urlSon.setText("null");
		
		el.addContent(id);
		el.addContent(nom);
		el.addContent(urlImage);
		el.addContent(urlSon);
		
		doc.setContent(racine);
		
		try {
			FileWriter writer = new FileWriter("bdd.xml");
			XMLOutputter outputter = new XMLOutputter();
			outputter.setFormat(Format.getPrettyFormat());
			outputter.output(doc, writer);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

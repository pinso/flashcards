package modele;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Scanner;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.Element;
/**
 * 
 * Cette classe incremente l'interface Deck.
 * Elle permet de regrouper toutes les cartes du jeu.
 * Elle permet egalement de melanger le jeu de carte et de generer un deck specialement pour chaque mode.
 *
 */
public class DeckCarte extends Observable implements Deck{
	
	/**
	 * Ces entiers donnent les informations sur l'etat de jeu.
	 * Dans l'ordre, le nombre de coup joue, le nombre de carte trouvee, le nombre d'erreur effectuee.
	 */
	private int nbCoupJoues, nbCartesTrouvees, nbErreur;
	/**
	 * Liste qui regroupe toutes les cartes du jeu.
	 */
	private ArrayList<Carte> cartes;
	/**
	 * Liste qui regroupe les cartes de la partie en cours.
	 */
	private ArrayList<Carte> deckcourant;
	/**
	 * Carte actuellement manipulee.
	 */
	private Carte carteCourante;
	/**
	 * Un entier qui represente l'id de la carte a trouver.
	 */
	private int cadeviner = -1;
	/**
	 * L'interface du jeu dans laquelle on fait la partie.
	 */
	private ModelJeu mj;
	/**
	 * Le nombre de carte dans la partie en cours.
	 */
	private int nbCarte;
	/**
	 * La carte son qui servira a recuperer les chemins absolus des fichiers son.
	 */
	private CarteSon cs = new CarteSon();
	/**
	 * Attribut necessaire a la lecture du fichier xml du jeu.
	 */
	private Element racine;
	/**
	 * Constructeur de deckcarte qui initialise le jeu et le contenu des listes.
	 */
	public DeckCarte() {
		this.nbCoupJoues = 0;
		this.nbCartesTrouvees = 0;
		this.nbErreur = 0;
		this.cartes = new ArrayList<Carte>();
		for(int i=0; i<retournerIdXML("bdd.xml"); i++) {
			this.cartes.add(new CarteImage());
		}
	}
	/**
	 * Methode qui permet de retourner l'id le plus grand parmis la liste des cartes.
	 * @param xml, une chaine de caractere representant le nom du fichier XML qui stocke les donnees du jeu.
	 * @return int
	 */
	public int retournerIdXML(String xml){
		SAXBuilder sxd = new SAXBuilder();
		
		Document doc = null;
		try {
			doc = sxd.build(new File(xml));
		} catch (JDOMException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		racine = doc.getRootElement();
		
		List carte = racine.getChildren("carte");
		Iterator i = carte.iterator();
		
		
		ArrayList<String> res = new ArrayList<String>();
		while(i.hasNext()) {
			Element courant = (Element)i.next();
			res.add(courant.getChild("id").getText());
		}
		return res.size();
	}
	
	
	/**
	 * Methode permettant de traiter le retournement d'une carte.
	 * @param i, entier correspondant a l'id de la carte a afficher.
	 */
	public void retournerCarte(int i) {
		for(int j=0;j<this.cartes.size();j++) {
			if(this.cartes.get(j).getIdCarte()==i) {	
				this.carteCourante = cartes.get(j);
			}
		}
		
		if (this.carteCourante.estVisible()) {
			this.carteCourante.setVisible(false);
		} else {
			this.carteCourante.setVisible(true);
		}
		this.setChanged();
		this.notifyObservers("Selection d'une carte");	
	}
	/**
	 * Methode permettant de mettre toutes les cartes cote recto.
	 */
	public void rectoCarte() {
		for(int j=0;j<this.deckcourant.size();j++) {
			this.deckcourant.get(j).setVisible(true);
		}
		this.setChanged();
		this.notifyObservers("Cartes retournées côté recto");	
	}
	/**
	 * Methode permettant de mettre toutes les cartes cote verso.
	 */
	public void versoCarte() {
		for(int j=0;j<this.deckcourant.size();j++) {
			this.deckcourant.get(j).setVisible(false);
		}
		this.setChanged();
		this.notifyObservers("Cartes retournées côté verso");	
	}
	/**
	 * Methode permettant d'initialiser la liste de carte generee aleatoirement pour une partie du mode image.
	 * @param i, le nombre de carte voulu dans le deck.
	 */
	public void chargerImage(int i) {
		this.nbCarte = i;
		ArrayList<Carte> copie = new ArrayList<Carte>();
		for(int k=0; k<retournerIdXML("bdd.xml"); k++) {
			copie.add(this.getCartes().get(k));
		}
		ArrayList<Carte> deckreturn = new ArrayList<Carte>();
		Collections.shuffle(copie);
		for(int j=0; j<i; j++) {
			deckreturn.add(copie.get(j));
		}
		this.deckcourant = deckreturn;
		this.setChanged();
		this.notifyObservers("Changer deck carte");	
	}
	/**
	 * Methode permettant d'initialiser la liste de carte generee aleatoirement pour une partie du mode son.
	 * @param i, le nombre de carte voulu dans le deck.
	 * @return int
	 */
	public int chargerSon(int i) {
		this.nbCarte = i;
		int[] listID = this.cs.getIDListSon();
		ArrayList<Carte> copie = new ArrayList<Carte>();
		for(int k=0; k<retournerIdXML("bdd.xml"); k++) {
			for (int l=0; l<listID.length; l++) {
				if(listID[l] == this.getCartes().get(k).getIdCarte()) {
					copie.add(this.getCartes().get(k));
				}
			}
		}
		ArrayList<Carte> deckreturn = new ArrayList<Carte>();
		Collections.shuffle(copie);
		for(int j=0; j<this.nbCarte; j++) {
			deckreturn.add(copie.get(j));
		}
		int carteatrouve = deckreturn.get(0).getIdCarte();
		Collections.shuffle(deckreturn);
		this.deckcourant = deckreturn;
		this.setChanged();
		this.notifyObservers("Changer deck carte");
		return carteatrouve;
	}
	
	/**
	 * Methode permettant de melanger le deck
	 */
	
	public void melanger() {
		Collections.shuffle(this.cartes);
		this.setChanged();
		this.notifyObservers("Melanger");
	}
	
	/**
	 * Methode permettant d'afficher le nombre de coup joues.
	 * @return un entier correspondant au nombre de coup joues.
	 */
	public int getNbCoupJoues() {
		return this.nbCoupJoues;
	}
	
	/**
	 * Methode permettant d'afficher le nombre de carte trouvees.
	 * @return un entier correspondant au nombre de cartes trouvees.
	 */
	public int getNbCartesTrouvees() {
		return this.nbCartesTrouvees;
	}
	
	/**
	 * Methode permettant de retourner la liste des cartes.
	 * @return une arraylist de cartes.
	 */
	public ArrayList<Carte> getCartes(){
		return this.cartes;
	}
	/**
	 * Methode permettant de recuperer la i eme carte du deckcourant.
	 * @param i, la position de la carte dans le deck.
	 * @return la carte a la position i
	 */
	public Carte getCarte(int i) {
		return this.deckcourant.get(i);
	}
	/**
	 * Methode qui retourne la carte actuellement manipulee.
	 * @return la carte qui est actuellement manipulee dans le deck pour le traitement.
	 */
	public Carte getCarteCourante() {
		return this.carteCourante;
	}
	/**
	 * Permet de definir la carte a deviner par son id.
	 * @param i, l'id de la carte a deviner.
	 */
	public void setDeviner(int i) {
		this.cadeviner = i;
	}
	/**
	 * Getter de l'id de la carte a deviner.
	 * @return l'id de la carte a deviner.
	 */
	public int getDeviner() {
		return this.cadeviner;
	}
	/**
	 * Permet de set le mode dans lequel on joue.
	 * @param model, l'interface dans lequel on joue.
	 */
	public void setModeActif(ModelJeu model) {
		this.mj = model;
	}
	/**
	 * Un getter qui retourne le mode dans lequel on joue.
	 * @return le mode dans lequel on est actuellement entrain de jouer
	 */
	public ModelJeu getModeActif() {
		return this.mj;
	}
	/**
	 * Methode void qui permet d'incrementer d'une unite le nombre de carte trouvee
	 */
	public void incrementCarteTrouvee() {
		nbCartesTrouvees++;
	}
	/**
	 * Methode qui permet d'incrementer le nombre d'erreur de un.
	 */
	public void incrementErreur() {
		nbErreur++;
	}
	/**
	 * Methode getter qui permet de retourner le nombre d'erreur.
	 * @return un entier representant le nombre d'erreur.
	 */
	public int getNbErreur() {
		return nbErreur;
	}
}


package modele;

import java.util.ArrayList;
/**
 * 
 * Cette classe implemente l'interface carte.
 * Elle represente la version son de l'objet Carte qui diffusera un son correspondant a la carte.
 * avec les autres methodes.
 *
 */
public class CarteSon implements Carte {
	/**
	 * Un entier qui represente l'id de la carte.
	 */
	protected int idCarte;
	/**
	 * @deprecated a ne pas prendre en compte puisque l'on ne peut pas retourner une carte son.
	 */
	private boolean visible;
	/**
	 * une liste qui regroupe tous les chemins absolu des fichiers sons sous forme de chaine de caractere.
	 */
	private ArrayList<String> listSon = new ArrayList<String>();
	/**
	 * une liste qui regroupe les id des cartes qui possedent des sons.
	 */
	private int[] listIDSon = {0,1,2,5,6,7,8};
	/**
	 * Constructeur de la classe qui ne requiert aucun parametre pour etre cree.
	 */
	public CarteSon() {
		this.idCarte=-1;
		this.visible = true;
		this.initList();
	}
	/**
	 * Methode setter qui permet de definir l'id d'une carte.
	 * @param i, un entier qui represente l'id que l'on voudrait attribuer a la carte.
	 */
	public void setIdCarte(int i) {
		this.idCarte=i;
	}
	/**
	 * Methode qui initialise la liste des sons existants en ajoutant le chemin absolu de leur son.
	 */
	public void initList() {
		this.listSon.add("son0.wav");
		this.listSon.add("son1.WAV");
		this.listSon.add("son2.WAV");
		this.listSon.add("son5.WAV");
		this.listSon.add("son6.wav");
		this.listSon.add("son7.WAV");
		this.listSon.add("son8.wav");
	}
	/**
	 * Methode qui permet de chercher la position dans le tableau associe a l'id de la carte 
	 * entree en parametre dans le tableau.
	 * @param i, l'id de la carte dont on voudrait connaitre la position dans le tableau.
	 * @return un entier representant la position de l'id de la carte dans le tableau listIDSon.
	 */
	public int rechercheId(int i) {
		boolean trv = false;
		int j = 0;
		while (j<7) {
			if(this.listIDSon[j]==i) {
				return j;
			}
			j++;
		}
		return -1;
	}
	/**
	 * Methode permettant d'ajouter un chemin absolu du fichier son dans la liste des chemins absolus des fichiers sons.
	 * @param str, une chaine de caractere representant le chemin absolu du fichier son que l'on voudrait ajouter a la liste.
	 */
	public void ajoutList(String str) {
		this.listSon.add(str);
	}
	/**
	 * Methode getter qui retourne la liste des chemins absolus du fichier son des cartes.
	 * @return l'arraylist de chaine de caractere representant les chemins absolus des fichiers sons.
	 */
	public ArrayList<String> getListSon() {
		return this.listSon;
	}
	/**
	 * Methode getter qui retourne le tableau qui regroupe les id des cartes possedant des sons.
	 * @return un tableau d'entier representant les id des cartes possedant des sons.
	 */
	public int[] getIDListSon() {
		return this.listIDSon;
	}
	/**
	 * Methode getter qui retourne l'id de la carte.
	 * @return un entier qui represente l'id de la carte.
	 */
	public int getIdCarte() {
		return this.idCarte;
	}
	/**
	 * @deprecated obsolete puisque l'on ne retourne pas une carte son.
	 */
	public void setVisible (boolean b) {
		this.visible=b;
	}
	
	/**
	 * Methode permettant de savoir si la carte est visible ou non.
	 * @return true si la carte est visible et false si la carte est cachee.
	 */
	public boolean estVisible() {
		return this.visible;
	}
	/**
	 * Methode getter qui retourne le chemin absolu de l'image de la carte son.
	 * @return String
	 */
	public String getCheminCarte() {
		return "src/img/son.png";
	}
	/**
	 * Methode getter qui retourne le chemin absolu du fichier son de la carte.
	 * @return String
	 */
	public String getCheminSon() {
		return "src/son/"+this.getCheminSonCarte();
	}
	/**
	 * Methode getter qui retourne le nom du fichier son de la carte associe a son id.
	 * @return String
	 */
	public String getCheminSonCarte() {
		return this.listSon.get(this.rechercheId(this.idCarte));
	}
	/**
	 * @deprecated obsolete car on ne nomme plus les cartes.
	 */
	@Override
	public String getNomCarte() {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * @deprecated obsolete car on ne nomme plus les cartes.
	 */
	@Override
	public void setNomCarte(String s) {
		// TODO Auto-generated method stub
		
	}
	
}
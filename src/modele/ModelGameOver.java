package modele;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Start;
/**
 * 
 * Interface qui affiche gameover.
 *
 */
public class ModelGameOver extends ModelJeu{
	
	private JPanel p;
	private JPanel p1;
	private JPanel p2;
	private JLabel perdu;
	/**
	 * Constructeur de l'interface.
	 */
	public ModelGameOver() {
		p = new JPanel();
		p1 = new JPanel();
		p2 = new JPanel();

		perdu = new JLabel("vous avez perdu !");
		
		p.setLayout(new BorderLayout());
		p1.setBorder(BorderFactory.createEmptyBorder(150, 0, 150, 0));
		p1.add(perdu);
		p.add(p1, BorderLayout.CENTER);
		p.add(p2, BorderLayout.SOUTH);
		this.add(p);
	}
}

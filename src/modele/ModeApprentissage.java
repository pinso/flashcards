package modele;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import controler.ControlerSouris;
import main.Start;
import vue.VueImage;
/**
 * 
 * Interface de jeu qui affiche toutes les cartes que l'on peut retourner
 *
 */
public class ModeApprentissage extends ModelJeu {
	/**
	 * GridLayout qui permet d'afficher 2 lignes de 5 cartes.
	 */
	private static GridLayout grille = new GridLayout(2,5);
	/**
	 * La vue des cartes.
	 */
	private VueImage vc;
	/**
	 * Le deck de cartes qu'on utilise pour le jeu.
	 */
	private DeckCarte dc;
	/**
	 * Les conteneurs qui contiennent les cartes et les boutons.
	 */
	private JPanel plateau, panelbouton;
	/**
	 * Le chemin absolu du fichier image de la carte.
	 */
	private String chemin;
	/**
	 * Le chemin absolu où se trouve les cartes.
	 */
	private String repertoire = "src/img";
	/**
	 * Une carte image independante pour recuperer les compteurs.
	 */
	private CarteImage cardcount;
	/**
	 * Attribut Element necessaire a la lecture du fichier XML.
	 */
	private Element racine;
	/**
	 * Un JButton afin d'afficher la prochaine (ou précédente) liste de carte qui ne sont pas encore affichees.
	 */
	private JButton bouton, bouton2;
	/**
	 * Le compteur qui permet de savoir combien de cartes ont ete deja affichees.
	 */
	private int compteur = 0;
	
	/**
	 * Constructeur de l'interface modeapprentissage.
	 * @param cc, la carte independante necessaire afin de recuperer le compteur
	 * @param css, le controleur que l'on utilise pour retourner les cartes
	 */
	public ModeApprentissage(CarteImage cc, ControlerSouris css) {
		/*************************************
		 * Creation des composants Model et controleurs.
		 *************************************/
		
		cardcount = cc;
		cardcount.reinit();
		
		dc = new DeckCarte();
		cs = css;
		cs.setDeck(dc);
		
		/*************************************
		 * Le JPanel plateau au nord dans lequel les cartes sont affichees.
		 *************************************/
		plateau = new JPanel();
		panelbouton = new JPanel();
		plateau.setLayout(grille);
		grille.setHgap(10);
		grille.setVgap(10);
		
		bouton = new JButton("Suivant");
		bouton2 = new JButton("Precedent");
		
		/*************************************
		 * Creation des JLabel associes aux cartes + affichage dans le JPanel plateau.
		 ************************************/
		for(int i = this.compteur; i<this.compteur+10; i++) {
			if(dc.getCartes().get(i).estVisible()) {
				chemin = repertoire + "/im" + dc.getCartes().get(i).getIdCarte()+".png";			
			} else {
				chemin = repertoire + "/fond" +dc.getCartes().get(i).getIdCarte()+".png";
			}
			vc = new VueImage(dc.getCartes().get(i), chemin);
			vc.addMouseListener(cs);
			dc.addObserver(vc);
			try {
				vc.setIcon(new ImageIcon(ImageIO.read(new File(chemin))));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			plateau.add(vc);
		}
		bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suivant();
			}
		});
		bouton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				precedent();
			}
		});
		panelbouton.add(bouton2);
		panelbouton.add(bouton);
		this.add(plateau, BorderLayout.NORTH);
		this.add(panelbouton, BorderLayout.SOUTH);
	}
	/**
	 * Methode qui permet d'afficher la prochaine liste de cartes.
	 */
	public void suivant() {
		plateau.removeAll();
		this.compteur+=10;
		for(int j = this.compteur; j<this.compteur+10; j++) {
			if (j<retournerIdXML("bdd.xml")) {
				if(dc.getCartes().get(j).estVisible()) {
					chemin = repertoire + "/im" + dc.getCartes().get(j).getIdCarte()+".png";			
				} else {
					chemin = repertoire + "/fond" +dc.getCartes().get(j).getIdCarte()+".png";
				}
				vc = new VueImage(dc.getCartes().get(j), chemin);
				vc.addMouseListener(cs);
				dc.addObserver(vc);
				try {
					vc.setIcon(new ImageIcon(ImageIO.read(new File(chemin))));
				} catch (IOException ew) {
					// TODO Auto-generated catch block
					ew.printStackTrace();
				}
				plateau.add(vc);
			} else {
				JLabel jp = new JLabel();
				try {
					jp.setIcon(new ImageIcon(ImageIO.read(new File(repertoire+"/fond.png"))));
				} catch (IOException ew) {
					// TODO Auto-generated catch block
					ew.printStackTrace();
				}
				plateau.add(jp);
			}
		}
		this.revalidate();
		this.repaint();
	}
	/**
	 * Methode qui permet d'afficher l'ancienne liste de cartes.
	 */
	public void precedent() {
		if (this.compteur>=10) {
			this.compteur-=20;
			this.suivant();
		}
	}
	/**
	 * Methode qui permet de retourner l'id le plus grand parmis la liste des cartes.
	 * @param xml, une chaine de caractere representant le nom du fichier XML qui stocke les donnees du jeu.
	 * @return int
	 */
	public int retournerIdXML(String xml){
		SAXBuilder sxd = new SAXBuilder();
		
		Document doc = null;
		try {
			doc = sxd.build(new File(xml));
		} catch (JDOMException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		racine = doc.getRootElement();
		
		List carte = racine.getChildren("carte");
		Iterator i = carte.iterator();
		
		
		ArrayList<String> res = new ArrayList<String>();
		while(i.hasNext()) {
			Element courant = (Element)i.next();
			res.add(courant.getChild("id").getText());
		}
		return res.size();
	}

}

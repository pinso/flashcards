package modele;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * Interface carte representant l'objet carte d'un jeu.
 * Les explications du choix des méthodes seront dans les classes qui implementent cet interface.
 *
 */

public interface Carte {
	/**
	 * Variable de type AtomicInteger dont nous nous servons comme compteur de carte.
	 * Il genere un entier qu'on stockera dans une variable.
	 */
	public static AtomicInteger genID = new AtomicInteger();
	/**
	 * Un getter qui permet de retourner l'id d'une carte.
	 * @return un entier qui represente l'id d'une carte.
	 */
	public int getIdCarte();
	/**
	 * @deprecated obsolete car nous ne nommons plus les cartes.
	 * Une methode qui retourne le nom d'une carte.
	 * @return une chaine de caractere, elle represente le nom de la carte.
	 */
	public String getNomCarte();
	/**
	 * @deprecated obsolete car nous ne nommons plus les cartes.
	 * Un setter qui permet de definir le nom d'une carte.
	 * @param s, une chaine de caractere qui represente le nom qu'on voudrait donner a la carte.
	 */
	public void setNomCarte(String s);
	/**
	 * une methode qui permet de choisir l'affichage que l'on veut de la carte, de face ou de dos.
	 * @param b, un boolean vrai pour la face d'une carte et faux pour le dos de la carte.
	 */
	public void setVisible (boolean b);
	/**
	 * Methode qui retourne l'etat d'affichage de la carte.
	 * @return un booleen vrai pour la face d'une carte et faux pour le dos de la carte.
	 */
	public boolean estVisible();
	/**
	 * un getter qui retourne le chemin absolu d'une carte
	 * @return une chaine de caractere representant le chemin absolu de la carte.
	 */
	public String getCheminCarte();
	
}

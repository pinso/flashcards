package modele;
import main.Start;

import javax.swing.JPanel;

import controler.ControlerSouris;
/**
 * 
 * Une classe abstraite qui herite de la classe JPanel.
 * Elle represente l'interface de jeu sans specialite.
 *
 */
public abstract class ModelJeu extends JPanel{
	/**
	 * Attribut jeu qui represente le main dans lequel la classe est utilisee.
	 */
	protected Start jeu;
	/**
	 * Le controleur utilisee pour recuperer les actions de la souris.
	 */
	protected ControlerSouris cs;
	/**
	 * Methode permettant de definir le main dans lequel la classe est utilisee.
	 * @param obj, la classe dans lequel l'interface est utilisee.
	 */
	public void setJeu(Start obj) {
		this.jeu = obj;
		this.cs.setJeu(jeu);
	}
	/**
	 * Getter qui retourne l'interface dans lequel la classe est utilisee.
	 * @return l'objet qui utilise cette classe.
	 */
	public Start getJeu() {
		return (Start)this.jeu;
	}
	
}

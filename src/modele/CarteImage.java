package modele;

/**
 * 
 * Cette classe implemente l'interface carte.
 * Elle represente la version image de l'objet Carte que l'on pourra retourner de dos ou de face.
 * avec les autres methodes.
 *
 */

public class CarteImage implements Carte {
	/**
	 * un entier qui represente l'id d'une carte.
	 */
	protected int idCarte;
	/**
	 * @deprecated obsolete car nous ne nommons plus les cartes.
	 * une chaine de caractere qui represente le nom de la carte.
	 */
	private String nomCarte;
	/**
	 * une variable booleene qui affiche la carte de face lorsqu'elle est vraie et de dos lorsqu'elle est fausse.
	 */
	private boolean visible;
	
	/**
	 * Constructeur de CarteImage qui ne prend aucun parametre et qui permet de creer l'objet CarteImage.
	 */
	
	public CarteImage() {
		this.idCarte=genID.getAndIncrement();
		this.visible = true;
	}
	
	/**
	 * Un getter qui retourne l'ID de l'objet CarteImage.
	 * @return Un entier qui représente l'ID de la carte.
	 */
	
	public int getIdCarte() {
		return this.idCarte;
	}
	
	/**
	 * @deprecated obsolete, le jeu n'utilise plus de nom de carte.
	 * Un getter qui retourne le nom de la carte.
	 * @return une chaine de caractere representant le nom de la carte.
	 */
	
	public String getNomCarte() {
		return this.nomCarte;
	}
	
	/**
	 * @deprecated obsolete car le jeu n'utilise plus de nom de carte.
	 * Un setter qui permet de definir le nom de la carte.
	 */
	
	public void setNomCarte(String s) {
		this.nomCarte=s;
	}
	
	/**
	 * @param b, un booleen qui permet de rendre la carte de face pour true et de dos pour false.
	 * Methode setter qui permet de choisir l'affichage de la carte de dos ou de face.
	 */
	
	public void setVisible (boolean b) {
		this.visible=b;
	}
	
	/**
	 * Methode permettant de savoir si la carte est visible ou non.
	 * @return true si la carte est visible et false si la carte est cachee.
	 */
	public boolean estVisible() {
		return this.visible;
	}
	
	/**
	 * Methode getter qui permet de recuperer le chemin absolu de la carte
	 */

	public String getCheminCarte() {
		return "src/img/im" + idCarte +".png";
	}
	
	/**
	 * Methode qui permet de remettre la variable public static genID de type AtomicInteger a 0.
	 */
	
	public void reinit() {
		genID.set(0);
	}
	
}

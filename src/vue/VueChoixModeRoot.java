package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
/**
 * @deprecated cette classe n'est pas utilisee dans le projet.
 */
public class VueChoixModeRoot {

	public VueChoixModeRoot() {
		JFrame f = new JFrame("FlashCard");
		JPanel p = new JPanel();
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		JPanel p4 = new JPanel();
		JPanel p5 = new JPanel();
		JPanel p6 = new JPanel();
		JPanel p7 = new JPanel();
		JPanel p8 = new JPanel();

		p.setLayout(new BorderLayout());
		p2.setLayout(new BorderLayout());
		p3.setLayout(new BorderLayout());
		p4.setLayout(new BorderLayout());
		p8.setLayout(new BorderLayout());

		p1.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
		p5.setBorder(BorderFactory.createEmptyBorder(20, 45, 20, 20));
		p6.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 45));
		p7.setBorder(BorderFactory.createEmptyBorder(20, 20, 10, 10));
		p8.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 0));

		f.setPreferredSize(new Dimension(500,400));
		
		JButton flash = new JButton("FLASHCARD");
		flash.setPreferredSize(new Dimension(150, 50));
		flash.setEnabled(false);
		
		JLabel choix = new JLabel("Choisis un mode de jeu");
		
		JButton i = new JButton("Image");
		i.setPreferredSize(new Dimension(160, 150));
		
		JButton s = new JButton("Son");
		s.setPreferredSize(new Dimension(160, 150));
		
		JButton e = new JButton("Editeur");
		e.setPreferredSize(new Dimension(100, 50));
		
		p7.add(e);
		p3.add(p7, BorderLayout.EAST);
		
		p5.add(i);
		p6.add(s);
		p4.add(p5, BorderLayout.WEST);
		p4.add(p6, BorderLayout.EAST);
		
		p1.add(flash);
		p8.add(choix, BorderLayout.WEST);
		
		p2.add(p8, BorderLayout.NORTH);
		p2.add(p4, BorderLayout.CENTER);
		
		f.add(p);
		p.add(p1, BorderLayout.NORTH);
		p.add(p2, BorderLayout.CENTER);
		p.add(p3, BorderLayout.SOUTH);
		
		i.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				f.dispose();
				VueChoixSousModes vcsm = new VueChoixSousModes("image");
				}
		});
		
		s.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				f.dispose();
				VueChoixSousModes vcsm = new VueChoixSousModes("son");
			}
			
		});
		
		e.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("a faire");
			}
			
		});
		
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
		f.setVisible(true);
	}
}

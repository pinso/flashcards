package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import controler.ControlerSouris;
import modele.*;
/**
 * @deprecated cette classe n'est pas utilisee dans le projet.
 */
public class VueChoixSousModes {
	
	private ControlerSouris css = new ControlerSouris();
	private CarteImage ci = new CarteImage();

	public VueChoixSousModes(String mode) {
		JFrame f = new JFrame("FlashCard");
		JPanel p = new JPanel();
		JPanel p1 = new JPanel();
		JPanel p3 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p4 = new JPanel();
		JPanel p5 = new JPanel();
		JPanel p6 = new JPanel();
		JPanel p7 = new JPanel();

		p.setLayout(new BorderLayout());
		p2.setLayout(new BorderLayout());
		p6.setLayout(new BorderLayout());

		p1.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
		p3.setBorder(BorderFactory.createEmptyBorder(20, 10, 20, 10));
		p4.setBorder(BorderFactory.createEmptyBorder(20, 10, 20, 10));
		p5.setBorder(BorderFactory.createEmptyBorder(20, 10, 20, 10));
		p6.setBorder(BorderFactory.createEmptyBorder(30, 10, 20, 0));

		f.setPreferredSize(new Dimension(500,400));

		JButton flash = new JButton("FLASHCARD");
		flash.setPreferredSize(new Dimension(150, 50));
		flash.setEnabled(false);

		JLabel choix = new JLabel("Choisis un mode de jeu");

		JButton entr = new JButton("Entrainement");
		entr.setPreferredSize(new Dimension(140, 130));

		JButton eval = new JButton("evaluation");
		eval.setPreferredSize(new Dimension(140, 130));

		JButton endur = new JButton("endurance");
		endur.setPreferredSize(new Dimension(140, 130));

		p1.add(flash);
		p3.add(entr);
		p4.add(eval);
		p5.add(endur);
		p6.add(choix, BorderLayout.WEST);

		p2.add(p6, BorderLayout.NORTH);
		p2.add(p3, BorderLayout.WEST);
		p2.add(p4, BorderLayout.CENTER);
		p2.add(p5, BorderLayout.EAST);

		f.add(p);
		p.add(p1, BorderLayout.NORTH);
		p.add(p2, BorderLayout.CENTER);

		entr.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switch(mode) {
				case "image":
					f.dispose();
					ModeImage mi = new ModeImage(5, ci, css);
					break;
				case "son":
					f.dispose();
					ModeSon ms = new ModeSon(5, ci, css);
					break;
				}
			}});

		eval.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switch(mode) {
				case "image":
					System.out.println("a faire");
					break;
				case "son":
					System.out.println("a faire");
					break;
				}
			}});
		
		endur.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switch(mode) {
				case "image":
					System.out.println("a faire");
					break;
				case "son":
					System.out.println("a faire");
					break;
				}
			}});


		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
		f.setVisible(true);
	}
}

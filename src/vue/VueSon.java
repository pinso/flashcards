package vue;

import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import modele.Carte;
import modele.DeckCarte;

public class VueSon extends JLabel implements VueCarte {
	
	private Carte carte;
	private DeckCarte test;
	private int idCarte;
	private String repertoire;
	
	public VueSon(Carte c, String r, int id) {
		this.carte = c;
		this.idCarte = id;
		this.repertoire = r;
	}


	public void update(Observable o, Object arg) {
		this.setIcon(new ImageIcon("src/img/son.png"));
	}
	
	/**
	 * Methode getter qui permet de recuperer la carte
	 * @return la carte
	 */
	public Carte getCarte() {
		return this.carte;
	}
	/**
	 * Un getter qui permet de retourner l'id d'une carte.
	 * @return un entier qui represente l'id d'une carte.
	 */
	public int getIdCarte() {
		return this.idCarte;
	}
}

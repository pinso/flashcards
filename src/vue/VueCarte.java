package vue;

import java.util.Observable;
import java.util.Observer;

import modele.Carte;

/**
 * 
 * Interface VueCarte
 *
 */
public interface VueCarte extends Observer{

	public void update(Observable o, Object arg);
	/**
	 * Methode getter qui permet de recuperer la carte
	 * @return la carte
	 */
	public Carte getCarte();
	/**
	 * Un getter qui permet de retourner l'id d'une carte.
	 * @return un entier qui represente l'id d'une carte.
	 */
	public int getIdCarte();
}

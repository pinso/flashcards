package vue;

import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import modele.Carte;
import modele.CarteImage;
import modele.CarteSon;
import modele.Deck;
import modele.DeckCarte;

public class VueImage extends JLabel implements VueCarte{
	
	private Carte carte;
	private int idCarte;
	private String repertoire;
	
	/**
	 * Constructeur de la vue VueImage
	 * @param c, carte
	 * @param r, repertoire
	 */
	public VueImage(Carte c, String r) {
		this.carte = c;
		this.idCarte = c.getIdCarte();
		this.repertoire = r;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (this.carte.estVisible()) {
				this.setIcon(new ImageIcon("src/img" + "/im" + this.carte.getIdCarte()+".png"));
			} else {
				this.setIcon(new ImageIcon("src/img" + "/fond" + this.carte.getIdCarte()+".png"));
			}
	}
		
	
	/**
	 * Methode getter qui permet de recuperer la carte courante
	 * @return la carte courante
	 */
	public Carte getCarte() {
		return this.carte;
	}
	/**
	 * Un getter qui permet de retourner l'id d'une carte.
	 * @return un entier qui represente l'id d'une carte.
	 */
	public int getIdCarte() {
		return this.idCarte;
	}
}

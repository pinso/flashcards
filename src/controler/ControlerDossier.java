package controler;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import org.jdom2.JDOMException;

import modele.Carte;
import modele.ModeEditeur;

/**
 * 
 * Cette classe incremente l'interface MouseListener
 * Elle permet gerer l'action de l'utilisateur quand il accede au mode Editeur 
 *
 */
public class ControlerDossier implements MouseListener{
	/**
	 * Attribut necessaire pour recuperer le chemin vers le fichier selectionne
	 */
	private JFileChooser url;
	/**
	 * Entier qui recupere le dernier id du Deck et qui est utilise pour generer les nouvelles cartes
	 */
	private static int compteur;
	/**
	 * Attribut necessaire pour recuperer le nom dans le JTextfield du mode Editeur
	 */
	protected ModeEditeur me;
	
	/**
	 * Constructeur de ControlerDossier pour creer l'objet
	 */
	public ControlerDossier() {
	}
	
	/**
	 * Methode appele lorsque l'utilisateur clique sur le JButton pour ajouter un nouveau fichier
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() instanceof JButton) {
			url = genererFichier();
			if (url != null) {
				this.CopierFichier();
				this.resizeImage();
				this.ecrireImage();
			} else {
				System.out.println("Pas de fichier selectionne");
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void setModelJeu(ModeEditeur modeediteur) {
		this.me = modeediteur;
	}
	
	/**
	 * Methode permettant a l'utilisateur de choisir un fichier de type .png ou .wav
	 * @return jfc correspond au selecteur de fichier
	 */
	public JFileChooser genererFichier(){
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setDialogTitle("Choisir un fichier");
		jfc.setAcceptAllFileFilterUsed(false);
		FileNameExtensionFilter png = new FileNameExtensionFilter("PNG images", "png");
		FileNameExtensionFilter jpeg = new FileNameExtensionFilter("JPEG images", "jpeg");
		FileNameExtensionFilter jpg = new FileNameExtensionFilter("JPG images", "jpg");
		FileNameExtensionFilter wav = new FileNameExtensionFilter("WAV songs", "wav");
		jfc.addChoosableFileFilter(png);
		jfc.addChoosableFileFilter(jpeg);
		jfc.addChoosableFileFilter(jpg);
		jfc.addChoosableFileFilter(wav);
		jfc.setPreferredSize(new Dimension(600,500));
		jfc.showOpenDialog(null);
		
		compteur = Carte.genID.getAndIncrement();
		me.ajouterElement(compteur);
		
		return jfc;
	}
	
	/**
	 * Methode permettant de copier le fichier selectionner et de le coller dans notre dossier image
	 */
	private void CopierFichier(){
		try (FileChannel in = new FileInputStream(url.getSelectedFile().getAbsoluteFile()).getChannel(); // canal d'entr�e
		        FileChannel out = new FileOutputStream("src/img/im"+compteur+".png").getChannel()) { // canal de sortie
		    // Copie depuis le in vers le out  
		    in.transferTo(0, in.size(), out);
		    } catch (IOException e) {
		        e.printStackTrace(); 
		    } 
	}
	
	/**
	 * Methode permettant de redimensionner une image passee en parametre
	 * @param img correspond a l'image que l'on souhaite redimensionner
	 * @param height correspond a la nouvelle hauteur de l'image
	 * @param width correspond a la nouvelle largeur de l'image
	 * @return l'image redimensionnee
	 */
	private static BufferedImage resize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }
	
	/**
	 * Methode permettant de redimensionner une image depuis notre fichier d'image
	 */
	public void resizeImage() {
		File input = new File("src/img/im"+compteur+".png");
	    BufferedImage image = null;
		try {
			image = ImageIO.read(input);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		BufferedImage resized = resize(image, 115, 115);
		File output = new File("src/img/im"+compteur+".png");
		try {
			ImageIO.write(resized, "png", output);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * Methode permettant d'ecrire le nom de la carte sur le fond
	 */
	public void ecrireImage() {
		File input = new File("src/img/fond.png");
		File output = new File("src/img/fond"+compteur+".png");
	    BufferedImage bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    Graphics graphics = bufferedImage.getGraphics();
	    graphics.setColor(Color.WHITE);
	    graphics.setFont(new Font("Arial Black", Font.BOLD, 20));
	    graphics.drawString(me.getNom(), bufferedImage.getHeight()/5, bufferedImage.getWidth()/2);
	    try {
			ImageIO.write(bufferedImage, "png", output);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    System.out.println("Image cree avec succes");
	 }
}

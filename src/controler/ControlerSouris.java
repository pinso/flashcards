package controler;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import main.Start;
import modele.CarteImage;
import modele.CarteSon;
import modele.DeckCarte;
import modele.ModeImage;
import modele.ModeSon;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import vue.VueCarte;

/**
 * 
 * Cette classe incremente MouseListener
 * Elle permet gerer l'action de l'utilisateur quand il clique sur une carte 
 *
 */
public class ControlerSouris implements MouseListener{
	/**
	 * Attribut jeu qui represente le main dans lequel la classe est utilisee.
	 */
	private Start jeu;
	/**
	 * Le deck de cartes que l'on utilisera pour le jeu.
	 */
	private DeckCarte dc;
	/**
	 * Carte independante pour recuperer les chemins absolus des fichiers son.
	 */
	private CarteSon cs = new CarteSon();
	
	/**
	 * Constructeur de ControlerSouris pour creer l'objet
	 */
	public ControlerSouris() {
	}
	
	/**
	 * Methode appele lorsque l'utilisateur clique sur une carte
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		VueCarte vc = (VueCarte)e.getSource();
		if (vc.getCarte() instanceof CarteImage) {
			dc.retournerCarte(vc.getCarte().getIdCarte());		
			if (dc.getDeviner()>-1 && this.dc.getModeActif() instanceof ModeImage) {
				if (vc.getIdCarte() == dc.getDeviner()) {
					dc.rectoCarte();
					((ModeImage)(this.dc.getModeActif())).nouvellePartie();
					dc.incrementCarteTrouvee();
					((ModeImage) jeu.getApp()).changerParam(dc.getNbErreur(), dc.getNbCartesTrouvees());
				} else {
					dc.incrementErreur();
					((ModeImage) jeu.getApp()).changerParam(dc.getNbErreur(), dc.getNbCartesTrouvees());
					if (dc.getNbErreur()>2) {
						jeu.gameOver();
					}
				}
			} else if (dc.getDeviner()>-1 && this.dc.getModeActif() instanceof ModeSon ) {
				if(vc.getIdCarte() == cs.getIDListSon()[cs.rechercheId(dc.getDeviner())]) {
					dc.rectoCarte();
					((ModeSon)(this.dc.getModeActif())).nouvellePartie();
					dc.incrementCarteTrouvee();
					((ModeSon) jeu.getApp()).changerParam(dc.getNbErreur(), dc.getNbCartesTrouvees());
				} else {
					dc.incrementErreur();
					((ModeSon) jeu.getApp()).changerParam(dc.getNbErreur(), dc.getNbCartesTrouvees());
					if (dc.getNbErreur()>2) {
						jeu.gameOver();
					}
				}
			}
		} else {
			genererSon((CarteSon)vc.getCarte());
		}
	}
	

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Methode permettant de generer un son 
	 * @param carte pour recuperer les chemins absolus des fichiers son.
	 */
	public void genererSon(CarteSon carte) {
		InputStream audioInputStream;
		try{
			audioInputStream = new FileInputStream(new File(carte.getCheminSon()));;
			AudioStream audio = new AudioStream(audioInputStream);
			AudioPlayer.player.start(audio);
		    } catch (IOException e) {
		            e.printStackTrace();
		            return;
		    }
	}
	/**
	 * Permet de set le deck courant
	 * @param dc, deck courant 
	 */
	public void setDeck(DeckCarte dc) {
		this.dc = dc;
	}
	/**
	 * Appelle la methode setJeu depuis l'interface de jeu.
	 * @param obj, interface Start 
	 */
	public void setJeu(Start obj) {
		this.jeu = obj;
	}

}

package main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import controler.ControlerDossier;
import controler.ControlerSouris;
import modele.CarteImage;
import modele.ModeApprentissage;
import modele.ModeEditeur;
import modele.ModeImage;
import modele.ModeSon;
import modele.ModelGameOver;
import modele.ModelJeu;

public class Start extends JFrame {
	/**
	 * Mode dans lequel on est entrain de jouer.
	 */
	private ModelJeu app;
	/**
	 * Conteneurs qui contiennent differentes parties du jeu.
	 */
	private JPanel p,p1,p2,p3,p4,p5,p6,p7,r1;
	/**
	 * Des labels pour mettre du textes.
	 */
	private JLabel login, carte, passw;
	/**
	 * Zone d'ecriture pour le nom d'utilisateur, nom de carte et mot de passe.
	 */
	private JTextField log, nomcarte, pass; 
	/**
	 * Bouton pour changer de mode et pour ajouter un fichier au jeu.
	 */
	private JButton b1, file;
	/**
	 * Compteur qui permet de changer de mode, 1 pour modeapprentissage, 2 pour modeson, 3 pour modeimage
	 */
	private int compteur = 0;
	/**
	 * Carte independante pour recuperer un compteur d'id.
	 */
	private CarteImage cardcount = new CarteImage();
	/**
	 * Controleur du modeediteur pour ajouter des cartes.
	 */
	private ControlerDossier cd = new ControlerDossier();
	/**
	 * Controleur pour retourner les cartes.
	 */
	private ControlerSouris cs = new ControlerSouris();
	
	/**
	 * Constructeur du jeu.
	 */
	public Start() {
		this.setTitle("Flashcards");
		p = new JPanel();
		p1 = new JPanel();
		p2 = new JPanel();
		p4 = new JPanel();
		p5 = new JPanel();
		p6 = new JPanel();
		p7 = new JPanel();
		r1 = new JPanel();
	
		this.setPreferredSize(new Dimension(500,400));
		
		login = new JLabel("Ecrit ton prenom puis clique sur le bouton START");
		log = new JTextField(30);
		
		passw = new JLabel("mot de passe");
		pass = new JTextField(30);
		
		b1 = new JButton("START");
		b1.setPreferredSize(new Dimension(100,50));
		
		p1.setLayout(new BorderLayout());
		p2.setLayout(new BorderLayout());
		//p3.setLayout(new FlowLayout());
		p4.setLayout(new FlowLayout());
		p5.setLayout(new FlowLayout());
		p6.setLayout(new FlowLayout());
		p7.setLayout(new FlowLayout());
		
		p4.add(login);
		p5.add(log);
		p1.add(p4, BorderLayout.NORTH);
		p1.add(p5, BorderLayout.SOUTH);
		
		p6.add(passw);
		p7.add(pass);
		p2.add(p6, BorderLayout.NORTH);
		p2.add(p7, BorderLayout.CENTER);
		
		p.setLayout(new BorderLayout());
		this.add(p);
		p.add(p1, BorderLayout.NORTH);
		this.add(b1, BorderLayout.SOUTH);
		b1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				compteur +=1;
				String id = log.getText();
				if (id.compareToIgnoreCase("root")==0) {
					p.add(p2, BorderLayout.CENTER);
					pack();
					String password = pass.getText();
					if (password.compareToIgnoreCase("root")==0) {
						p.removeAll();
						b1.setVisible(false);
						p.repaint();
						pack();
						
						app = new ModeEditeur(cardcount, cd);
						setPreferredSize(new Dimension(500,150));
						
						p.removeAll();
						p.add(app);
						p.revalidate();
						p.repaint();
						pack();
					}
				}
				else {
					if (compteur > 2) {
						app = new ModeImage(5, cardcount, cs);
						setPreferredSize(new Dimension(650,370));
					}
					if (compteur == 2) {
						app = new ModeSon(5, cardcount, cs);
						setPreferredSize(new Dimension(650,370));
					} else {
						if (compteur == 1) {
							app = new ModeApprentissage(cardcount, cs);
							setPreferredSize(new Dimension(650,380));
						}
					}
					setJeu();
					p.removeAll();
					p.add(app);
					p.revalidate();
					p.repaint();
					pack();
				}
			}
		});
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		setResizable(true);
		setLocationRelativeTo(null);
	}
	/**
	 * Retourne l'interface de jeu actuellement utilisee.
	 * @return app
	 */
	public ModelJeu getApp() {
		return this.app;
	}
	/**
	 * Appelle la methode setJeu depuis l'interface de jeu actuellement utilisee.
	 */
	public void setJeu() {
		app.setJeu(this);
	}
	/**
	 * Affiche l'interface gameover.
	 */
	public void gameOver() {
		p.removeAll();
		p.add(new ModelGameOver());
		p.revalidate();
		p.repaint();
		pack();
		compteur = 0;
	}
	/**
	 * Lance le jeu.
	 * @param args
	 */
	public static void main(String[] args) {
		Start start = new Start();
	}
}

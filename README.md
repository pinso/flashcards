# README #

Ce README est un document informatif qui vous donnera le fonctionnement de l'application.

### Sommaire ###

* Comment lancer l'application depuis Eclipse
* Quelles fonctionnalités propose l'application
* Contact

### Comment lancer l'application depuis Eclipse ###

* Depuis l'interface, lancer Start.java qui se trouve dans le repertoire src/main
* La compilation se fera toute seule si votre Eclipse est bien configuré.
* Les configurations requises :
* Java SE9
* La librairie jdom-2.0.6.jar ainsi que la librairie sun.audio
* Pourquoi la librairie jdom-2.0.6.jar ?
* Le jeu utilise la lecture des fichiers xml qui stocke toutes les données du jeu.
* Afin de permettre le bon fonctionnement de l'application, elle est requise pour lancer le jeu.
* Pourquoi la librairie sun.audio ?
* Cette librairie permet de faire fonctionner la lecture des fichiers audios .WAV
* Sans celle-ci, vous ne profiterez pas du mode ModeSon que propose FlashCards.
* A noter que le mode ModeImage fonctionnera parfaitement sans cette librairie.

### Quelles fonctionnalités propose l'application ###

* Après avoir suivi le tutoriel, le jeu devrait être lancé sans difficulté.
* Vous constaterez un écran qui demande votre nom d'utilisateur.
* Vous avez la liberté de choisir le pseudonyme qui vous convient, à noter qu'il n'y a aucune importante puisque cette version du jeu ne comporte pas la sauvegarde de profil.
* Vous avez la possibilité d'accéder au mode ModeEditeur qui vous permet d'accéder à la fonction Ajout de carte qui vous permet d'ajouter une carte au jeu.
* Pour cela il faudra saisir root en tant que pseudonyme.
* En confirmant, une zone de texte "Mot de passe" s'affichera, il faudra saisir root à nouveau afin d'accéder au mode ModeEditeur.
* L'interface Editeur s'affiche.
* Il faudra saisir le nom de la carte (Exemple : le nom d'un animal) et d'y mettre l'image correspondant en cliquant sur le logo dossier qui ouvrira votre explorateur de fichier.
* Selectionner l'image et confirmer.
* La console vous confirmera la création de l'image et de l'ajout dans le jeu.
* Il faut savoir que l'ajout est permanente donc il faudra être extrêmement rigoureux lors de la manipulation.
* Si vous n'avez pas choisi le mode ModeEditeur, c'est que vous souhaitez accéder aux modes de jeu.
* En saisissant un pseudonyme, le jeu va vous afficher l'interface ModeApprentissage.
* Cette inferface affiche une lignée de carte, il est possible de les retourner par un clic de souris.
* Au dos de ces cartes se trouve le nom associé à l'image. Dans notre cas, le nom d'un animal.
* Il est possible de défiler le reste des cartes par le bouton Suivant.
* Pour passer au mode ModeSon, il faudra cliquer sur le bouton Start.
* Une carte principale s'affiche avec un logo de son avec une lignée de carte en dessous de celle-ci
* En cliquant sur le logo de son, un son sera émis (si vous avez bien ajouté la librairie comme recommandé au dessus).
* Il faudra deviner l'animal associé au son émis et donner la bonne réponse en choisissant la carte associée dans la lignée en dessous.
* Il faut savoir qu'au bout de 3 erreurs, le jeu affiche l'interface GameOver et il faudra partir du début, à savoir le mode ModeApprentissage.
* En cliquant sur Start à nouveau, vous arriverez au mode ModeImage du jeu.
* Le principe est le même mais sans le son.
* Il faudra deviner la carte associé à ce nom d'animal affiché et cliquer sur la bonne réponse en dessous.
* Enjoy !

### Contact ###

* Author : Hoang Anh NGUYEN(1), Hugo PISANO(2), Théo Cerisara, Thomas ZUNINO
* (1) hoang-anh.nguyen1@etu.univ-lorraine.fr
* (2) hugo.pisano7@etu.univ-lorraine.fr
